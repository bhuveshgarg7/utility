package com.example.model;

import java.util.List;

public class MergeRequestData {

    private String mergeRequestUrl;
    private String conflictFile;
    private List<CommitDetail> commitDetails;
    
	public String getMergeRequestUrl() {
		return mergeRequestUrl;
	}
	public void setMergeRequestUrl(String mergeRequestUrl) {
		this.mergeRequestUrl = mergeRequestUrl;
	}
	public String getConflictFile() {
		return conflictFile;
	}
	public void setConflictFile(String conflictFile) {
		this.conflictFile = conflictFile;
	}
	public List<CommitDetail> getCommitDetails() {
		return commitDetails;
	}
	public void setCommitDetails(List<CommitDetail> commitDetails) {
		this.commitDetails = commitDetails;
	}
}

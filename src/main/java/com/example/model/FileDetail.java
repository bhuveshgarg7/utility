package com.example.model;

import java.util.List;

public class FileDetail {

	private String repoUrl;
	private List<CommitDetail> commitDetails;
	
	public String getRepoUrl() {
		return repoUrl;
	}
	public void setRepoUrl(String repoUrl) {
		this.repoUrl = repoUrl;
	}
	public List<CommitDetail> getCommitDetails() {
		return commitDetails;
	}
	public void setCommitDetails(List<CommitDetail> commitDetails) {
		this.commitDetails = commitDetails;
	}
}

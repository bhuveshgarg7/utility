package com.example.model;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

@Component
public class ExcelWriter {
	public void mergeRequestWithMergeConflicts(List<MergeRequestData> mergeRequests, String filePath)
			throws IOException {
		try (Workbook workbook = new XSSFWorkbook()) {
			Sheet sheet = workbook.createSheet("Merge Requests");

			Row headerRow = sheet.createRow(0);
			String[] columns = { "MR_With_Merged_Conflicts", "Conflict_File", "Commit_ID", "Commit_Time", "Author_Name",
					"Author_Email" };
			CellStyle style = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setBold(true);

			style.setFont(font);
			for (int i = 0; i < columns.length; i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(columns[i]);
				cell.setCellStyle(style);
			}

			int rowNum = 1;
			for (MergeRequestData mergeRequest : mergeRequests) {
				Row row = sheet.createRow(rowNum++);
				row.createCell(0).setCellValue(mergeRequest.getMergeRequestUrl());
				row.createCell(1).setCellValue(mergeRequest.getConflictFile());

				for (CommitDetail commitDetail : mergeRequest.getCommitDetails()) {
					row.createCell(2).setCellValue(commitDetail.getCommitId());
					row.createCell(3).setCellValue(commitDetail.getCommitDate());
					row.createCell(4).setCellValue(commitDetail.getAuthorName());
					row.createCell(5).setCellValue(commitDetail.getAuthorEmail());
					row = sheet.createRow(rowNum++);
				}

			}

			try (FileOutputStream fileOut = new FileOutputStream(filePath)) {
				workbook.write(fileOut);
			}
		}
	}

	public void fileDetail(String file, List<FileDetail> fileDetailList, String filePath) throws IOException {

		try (Workbook workbook = new XSSFWorkbook()) {
			Sheet sheet = workbook.createSheet("File Details for " + file);

			Row headerRow = sheet.createRow(0);
			String[] columns = { "Repo", "Author_Name", "Commit_Time", "Author_Email" };
			CellStyle style = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setBold(true);

			style.setFont(font);
			for (int i = 0; i < columns.length; i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(columns[i]);
				cell.setCellStyle(style);
			}

			int rowNum = 1;
			for (FileDetail fileDetail : fileDetailList) {
				Row row = sheet.createRow(rowNum++);
				row.createCell(0).setCellValue(fileDetail.getRepoUrl());

				for (CommitDetail commitDetail : fileDetail.getCommitDetails()) {
					row.createCell(1).setCellValue(commitDetail.getAuthorName());
					row.createCell(2).setCellValue(commitDetail.getCommitDate());
					row.createCell(3).setCellValue(commitDetail.getAuthorEmail());
					row = sheet.createRow(rowNum++);
				}
			}
			try (FileOutputStream fileOut = new FileOutputStream(filePath)) {
				workbook.write(fileOut);
			}
		}
	}

	public void jenkinsBuildData(List<JenkinsBuildDTO> jenkinsBuildDTOs, String outputExcelPath) throws FileNotFoundException, IOException {
		try (Workbook workbook = new XSSFWorkbook()) {
			Sheet sheet = workbook.createSheet("Jenkins Build Status");

			Row headerRow = sheet.createRow(0);
			String[] columns = { "Name", "Last_Success", "Last_Failure", "Status", "Failure_Reason" };
			CellStyle style = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setBold(true);

			style.setFont(font);
			for (int i = 0; i < columns.length; i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(columns[i]);
				cell.setCellStyle(style);
			}

			int rowNum = 1;
			for (JenkinsBuildDTO jenkinsBuildDTO : jenkinsBuildDTOs) {
				Row row = sheet.createRow(rowNum++);
				row.createCell(0).setCellValue(jenkinsBuildDTO.getJobName());
				row.createCell(1).setCellValue(jenkinsBuildDTO.getLastSuccess());
				row.createCell(2).setCellValue(jenkinsBuildDTO.getLastFailure());
				row.createCell(3).setCellValue(jenkinsBuildDTO.getStatus());
				row.createCell(4).setCellValue(jenkinsBuildDTO.getFailureReason());

			}
			
			// Adding sheet for failed jobs
	        Sheet failedJobsSheet = workbook.createSheet("Failed Jobs");
	        Row failedHeaderRow = failedJobsSheet.createRow(0);
	        String[] failedColumns = { "Name", "Last_Success", "Last_Failure", "Status", "Failure_Reason" };
	        for (int i = 0; i < failedColumns.length; i++) {
	            Cell cell = failedHeaderRow.createCell(i);
	            cell.setCellValue(failedColumns[i]);
	            cell.setCellStyle(style);
	        }
	        int failedRowNum = 1;
	        for (JenkinsBuildDTO jenkinsBuildDTO : jenkinsBuildDTOs) {
	            if ("FAILURE".equals(jenkinsBuildDTO.getStatus())) {
	                Row row = failedJobsSheet.createRow(failedRowNum++);
	                row.createCell(0).setCellValue(jenkinsBuildDTO.getJobName());
	                row.createCell(1).setCellValue(jenkinsBuildDTO.getLastSuccess());
	                row.createCell(2).setCellValue(jenkinsBuildDTO.getLastFailure());
	                row.createCell(3).setCellValue(jenkinsBuildDTO.getStatus());
	                row.createCell(4).setCellValue(jenkinsBuildDTO.getFailureReason());
	            }
	        }
			try (FileOutputStream fileOut = new FileOutputStream(outputExcelPath)) {
				workbook.write(fileOut);
			}
		}
	}

	public void sonarFileDetail(List<FileDetail> fileDetailList, String outputExcelPath) throws FileNotFoundException, IOException {


		try (Workbook workbook = new XSSFWorkbook()) {
			Sheet sheet = workbook.createSheet("Sonar Issue File Details");

			Row headerRow = sheet.createRow(0);
			String[] columns = {"Author_Name", "Commit_Time", "Author_Email" };
			CellStyle style = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setBold(true);

			style.setFont(font);
			for (int i = 0; i < columns.length; i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(columns[i]);
				cell.setCellStyle(style);
			}

			int rowNum = 1;
			for (FileDetail fileDetail : fileDetailList) {
				Row row = sheet.createRow(rowNum++);

				for (CommitDetail commitDetail : fileDetail.getCommitDetails()) {
					row.createCell(1).setCellValue(commitDetail.getAuthorName());
					row.createCell(2).setCellValue(commitDetail.getCommitDate());
					row.createCell(3).setCellValue(commitDetail.getAuthorEmail());
					row = sheet.createRow(rowNum++);
				}
			}
			try (FileOutputStream fileOut = new FileOutputStream(outputExcelPath)) {
				workbook.write(fileOut);
			}
		}
	
	}
}

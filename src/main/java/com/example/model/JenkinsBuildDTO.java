package com.example.model;

public class JenkinsBuildDTO {

    private String jobName;
    private String jobRelativeUrl;
    private String status;
    private String failureReason;
    private String lastSuccess;
    private String lastFailure;
    
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	
	public String getJobRelativeUrl() {
		return jobRelativeUrl;
	}
	public void setJobRelativeUrl(String jobRelativeUrl) {
		this.jobRelativeUrl = jobRelativeUrl;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFailureReason() {
		return failureReason;
	}
	public void setFailureReason(String failureReason) {
		this.failureReason = failureReason;
	}
	public String getLastSuccess() {
		return lastSuccess;
	}
	public void setLastSuccess(String lastSuccess) {
		this.lastSuccess = lastSuccess;
	}
	public String getLastFailure() {
		return lastFailure;
	}
	public void setLastFailure(String lastFailure) {
		this.lastFailure = lastFailure;
	}
    
    
}

package com.example.model;

public class CommitDetails {

	private int totalLineChanges;
	private String commitDetail;
	
	public int getTotalLineChanges() {
		return totalLineChanges;
	}
	public void setTotalLineChanges(int totalLineChanges) {
		this.totalLineChanges = totalLineChanges;
	}
	public String getCommitDetail() {
		return commitDetail;
	}
	public void setCommitDetail(String commitDetail) {
		this.commitDetail = commitDetail;
	}
}

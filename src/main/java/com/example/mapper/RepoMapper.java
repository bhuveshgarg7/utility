//package com.example.mapper;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.stereotype.Service;
//
//@Service
//@ConfigurationProperties(prefix = "git.repositories")
//public class RepoMapper {
//
//    private final List<String> repositoryUrls;
//
//    public RepoMapper(@Value("${git.repositories}") List<String> repositoryUrls) {
//        this.repositoryUrls = repositoryUrls;
//    }
//
//    public List<String> getRepositoryUrls() {
//        return repositoryUrls;
//    }
//}
package com.example.service;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;



import com.example.model.CommitDetails;

@Service
public class GitCommitDetailService {

	@Value("${gitlab.api.url}")
	private String gitlabApiUrl;

	@Value("${gitlab.access.token}")
	private String accessToken;

	public CommitDetails getCommitsForAuthor(String repoUrl, String authorUsername, String branch, String fromDate,
			String toDate) throws Exception {
		CommitDetails commitDetails = new CommitDetails();
		int projectId = getProjectId(repoUrl);
		String apiUrl = gitlabApiUrl + "/projects/" + projectId + "/repository/commits?ref_name=" + branch
				+ "&author_username=" + authorUsername + "&since=" + fromDate + "&until=" + toDate;
		HttpClient httpClient = HttpClients.createDefault();
		HttpGet request = new HttpGet(apiUrl);
		request.setHeader("Private-Token", accessToken);

		String responseBody = EntityUtils.toString(httpClient.execute(request).getEntity());
		JSONArray commitsArray = new JSONArray(responseBody);
		int countLines = 0;
		for (int i = 0; i < commitsArray.length(); i++) {
			JSONObject commit = commitsArray.getJSONObject(i);
			String commitId = commit.getString("id");
			countLines += getCommittedLines(projectId, commitId);
		}
		commitDetails.setTotalLineChanges(countLines);
		commitDetails.setCommitDetail(responseBody);
		return commitDetails;
	}

	private int getCommittedLines(int projectId, String commitId) throws Exception {
		String apiUrl = gitlabApiUrl + "/projects/" + projectId + "/repository/commits/" + commitId + "/diff";
		HttpClient httpClient = HttpClients.createDefault();
		HttpGet request = new HttpGet(apiUrl);
		request.setHeader("Private-Token", accessToken);

		String responseBody = EntityUtils.toString(httpClient.execute(request).getEntity());
		JSONArray commitsArray = new JSONArray(responseBody);

		int linesAdded = 0, linesRemoved = 0;
		for (int i = 0; i < commitsArray.length(); i++) {
			JSONObject jsonObject = commitsArray.getJSONObject(i);
			String diff = jsonObject.getString("diff");
			String[] lines = diff.split("\\n");
			for (String line : lines) {
				if (line.startsWith("+")) {
					linesAdded++;
				} else if (line.startsWith("-")) {
					linesRemoved++;
				}
			}
		}
		return linesAdded + linesRemoved;
	}

	private int getProjectId(String repoUrl) throws Exception {
		HttpClient httpClient = HttpClients.createDefault();
		String[] url = repoUrl.split("/");
		String apiUrl = gitlabApiUrl + "/projects/" + url[url.length - 2] + "%2F" + url[url.length - 1];
		HttpGet request = new HttpGet(apiUrl);
		request.setHeader("Private-Token", accessToken);

		String responseBody = EntityUtils.toString(httpClient.execute(request).getEntity());
		JSONObject responseJson = new JSONObject(responseBody);
		return responseJson.getInt("id");
	}
}

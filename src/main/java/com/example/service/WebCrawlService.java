package com.example.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import com.example.model.ExcelWriter;
import com.example.model.JenkinsBuildDTO;

@Service
public class WebCrawlService {

	private static final String OUTPUT_EXCEL_PATH = "D:\\demo1\\jenkinsReport.xlsx";

	@Value("${jenkins.username}")
	private String jenkinsUsername;

	@Value("${jenkins.password}")
	private String jenkinsPassword;

	@Autowired
	private ExcelWriter excelWriter;

	public void getContent(String url, LocalDate startDate, LocalDate endDate) throws URISyntaxException {
		List<JenkinsBuildDTO> jenkinsBuildDTOs = new ArrayList<>();
		try {

			Connection.Response response = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(10 * 1000)
					.ignoreHttpErrors(true).ignoreContentType(true).method(
							Connection.Method.GET)
					.header("Authorization", "Basic " + java.util.Base64.getEncoder()
							.encodeToString((jenkinsUsername + ":" + jenkinsPassword).getBytes()))
					.execute();

//            Document doc = Jsoup.connect(url).get();
			Document doc = response.parse();
//			System.out.println(doc);

			Element table = doc.select("table#projectstatus").first();
			System.out.println(table);
			if (table != null) {
				Elements rows = table.select("tr"); // Get all rows in the table
				for (Element row : rows) {
					JenkinsBuildDTO jenkinsBuildDTO = new JenkinsBuildDTO();
//			    	System.out.println("row -> "+row);
					/*
					 * Elements thElements = row.select("th"); for (Element th : thElements) {
					 * String thText = th.text();
					 * System.out.print(thText+"                           "); }
					 * System.out.println();
					 */
					Elements cells = row.select("td"); // Get all cells in each row
					if (!cells.isEmpty()) {
						String name = cells.get(2).select("a").attr("href");
						String lastSuccessString = cells.get(3).attr("data");
						String lastFailureString = cells.get(4).attr("data");

						DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX");
						LocalDate lastSuccessDate = null;
						if (!"-".equals(lastSuccessString))
						lastSuccessDate = LocalDate.parse(lastSuccessString,
								formatter);
						LocalDate lastFailureDate = null;
						if (!"-".equals(lastFailureString))
						lastFailureDate = LocalDate.parse(lastFailureString,
								formatter);

//	                    String datePart = dateTime.toLocalDate().toString();

						if (lastSuccessDate!=null && lastSuccessDate.isBefore(endDate) && lastSuccessDate.isAfter(startDate)
								|| lastFailureDate!=null && lastFailureDate.isBefore(endDate) && lastFailureDate.isAfter(startDate)) {
							jenkinsBuildDTO.setJobName(cells.get(2).text());
							jenkinsBuildDTO.setLastSuccess(cells.get(3).attr("data"));
							jenkinsBuildDTO.setLastFailure(cells.get(4).attr("data"));
							jenkinsBuildDTO.setJobRelativeUrl(cells.get(2).select("a").attr("href"));
							jenkinsBuildDTOs.add(jenkinsBuildDTO);
						}

//			            String cellText = cell.text();
//			            System.out.println("cell -> "+cell);
//			            System.out.print(cellText+"                     ");
						// Process or store the cell text according to your use case

					}
				}
			}
			System.out.println();

//			Elements jobLinks = doc.select("a.jenkins-jobs-list__item__details[href^=job/]");

			for (JenkinsBuildDTO jenkinsBuildDTO : jenkinsBuildDTOs) {
				// Get the relative URL
//				String relativeUrl = jobLink.attr("href");
				String relativeUrl = jenkinsBuildDTO.getJobRelativeUrl();
				System.out.println("Job -> " + relativeUrl);

				URI baseUri = new URI(url);

				// append the relative URL + last build output"
				URI absoluteUri = baseUri.resolve(relativeUrl + "lastBuild/consoleText");

//				System.out.println("Job Link: " + absoluteUrl);
				// Call the method to fetch content from the absolute URL
				getContentFromAbsoluteUrl(absoluteUri.toString(), jenkinsBuildDTO);
				System.out.println();
//				jenkinsBuildDTOs.add(jenkinsBuildDTO);
			}
			Files.deleteIfExists(Path.of(OUTPUT_EXCEL_PATH));
			excelWriter.jenkinsBuildData(jenkinsBuildDTOs, OUTPUT_EXCEL_PATH);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void getContentFromAbsoluteUrl(String absoluteUrl, JenkinsBuildDTO jenkinsBuildDTO) {
		try {
			Connection.Response response = Jsoup.connect(absoluteUrl).userAgent("Mozilla/5.0").timeout(10 * 1000)
					.ignoreHttpErrors(true).ignoreContentType(true).method(
							Connection.Method.GET)
					.header("Authorization", "Basic " + java.util.Base64.getEncoder()
							.encodeToString((jenkinsUsername + ":" + jenkinsPassword).getBytes()))
					.execute();

			Document doc = response.parse();
//			System.out.println(doc);

			Element body = doc.body();

			String bodyText = body.text();

			// Check if the status is SUCCESS or FAILURE
			if (bodyText.contains("Finished: SUCCESS")) {
				System.out.println("Status: SUCCESS");
				jenkinsBuildDTO.setStatus("SUCCESS");
			} else if (bodyText.contains("Finished: FAILURE")) {
				System.out.println("Status: FAILURE");
				jenkinsBuildDTO.setStatus("FAILURE");
				// Extract the reason for failure
				String regex = "ERROR: (.+)";
				Pattern pattern = Pattern.compile(regex);
				Matcher matcher = pattern.matcher(bodyText);

//				int startIndex = bodyText.indexOf("Finished: FAILURE") + "Finished: FAILURE".length();
//				String failureReason = bodyText.substring(startIndex).trim();
				if (matcher.find())
					System.out.println("Failure Reason: " + matcher.group(1).trim());
				jenkinsBuildDTO.setFailureReason(matcher.group(1).trim());
			} else {
				System.out.println("Unknown status");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
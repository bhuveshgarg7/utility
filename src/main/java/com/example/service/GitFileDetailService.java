package com.example.service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.model.CommitDetail;
import com.example.model.ExcelWriter;
import com.example.model.FileDetail;

@Service
public class GitFileDetailService {
	
	private static final String OUTPUT_EXCEL_PATH = "D:\\demo1\\output2.xlsx";

	@Value("${gitlab.api.url}")
	private String gitlabApiUrl;

	@Value("${gitlab.access.token}")
	private String accessToken;

	@Value("#{'${list.of.repos}'.split(',')}") 
	private List<String> repoUrlToLocalPathMap;

	@Autowired
	private ExcelWriter excelWriter;

	public void getFileDetail(String filePath, String branchName, int lastCommits) throws Exception {
		List<FileDetail> fileDetailList = new ArrayList<>();
		Map<String, String> repoUrlToLocalPathMap = new HashMap<>();
		for (String repoUrl : repoUrlToLocalPathMap.keySet()) {
			FileDetail fileDetail = new FileDetail();
			fileDetail.setRepoUrl(repoUrl);
			int projectId = getProjectId(repoUrl);
			fileDetail.setCommitDetails(getLatestCommitAuthor(projectId, filePath, lastCommits, branchName));
			fileDetailList.add(fileDetail);
		}
		Files.deleteIfExists(Path.of(OUTPUT_EXCEL_PATH));
		excelWriter.fileDetail(filePath, fileDetailList, OUTPUT_EXCEL_PATH);
	}

	private List<CommitDetail> getLatestCommitAuthor(int projectId, String filePath, int lastcommits, String branch)
			throws Exception {
		List<CommitDetail> commitDetails = new ArrayList<>();
		HttpClient httpClient = HttpClients.createDefault();
		String apiUrl = gitlabApiUrl + "/projects/" + projectId + "/repository/commits?ref_name=" + branch + "&path="
				+ filePath + "&per_page=" + lastcommits;
		HttpGet request = new HttpGet(apiUrl);
		request.setHeader("Private-Token", accessToken);

		String responseBody = EntityUtils.toString(httpClient.execute(request).getEntity());
		JSONArray commitsArray = new JSONArray(responseBody);

		for (int i = 0; i < commitsArray.length(); i++) {
			JSONObject commit = commitsArray.getJSONObject(i);
			String authorName = commit.getString("author_name");
			String date = commit.getString("committed_date");
			String authorEmail = commit.getString("committed_date");

			CommitDetail commitDetail = new CommitDetail();
			commitDetail.setAuthorName(commit.getString("author_name"));
			commitDetail.setCommitDate(commit.getString("committed_date"));
			commitDetail.setAuthorEmail(commit.getString("author_email"));
			commitDetails.add(commitDetail);
			System.out.println("Conflict file: " + filePath);
			System.out.println("Last commits:");
			System.out.println(", Commit Time : " + date + ", Author Name: " + authorName + "Author Email: " + authorEmail);
		}

		return commitDetails;
	}

	private int getProjectId(String repoUrl) throws Exception {
		HttpClient httpClient = HttpClients.createDefault();
		String[] url = repoUrl.split("/");
		String apiUrl = gitlabApiUrl + "/projects/" + url[url.length - 2] + "%2F" + url[url.length - 1];
		HttpGet request = new HttpGet(apiUrl);
		request.setHeader("Private-Token", accessToken);

		String responseBody = EntityUtils.toString(httpClient.execute(request).getEntity());
		JSONObject responseJson = new JSONObject(responseBody);
		return responseJson.getInt("id");
	}

}

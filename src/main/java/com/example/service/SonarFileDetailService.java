package com.example.service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.model.CommitDetail;
import com.example.model.ExcelWriter;
import com.example.model.FileDetail;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SonarFileDetailService {
	
	private static final String OUTPUT_EXCEL_PATH = "D:\\demo1\\output2.xlsx";

	@Value("${sonar.username}")
	private String username;

	@Value("${sonar.password}")
	private String password;

	@Value("${sonar.baseurl}")
	private String baseUrl;
	
	@Value("${gitlab.api.url}")
	private String gitlabApiUrl;

	@Value("${gitlab.access.token}")
	private String accessToken;
	
	@Autowired
	private ExcelWriter excelWriter;

	public void getFileDetail(String repoUrl, String projectKey, String branch, int lastCommits) throws Exception {

		// Constructing URL
		String url = baseUrl + "/api/issues/search?componentKeys=" + projectKey + "&branch=" + branch;

		// Creating HTTP Headers with Authorization header
		HttpHeaders headers = new HttpHeaders();
		headers.setBasicAuth(username, password);

		// Creating HTTP Entity with headers
		HttpEntity<String> entity = new HttpEntity<>(headers);

		// Making REST call
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

		List<String> sonarIssueFiles = new ArrayList<>();
		// Handle response here as per your requirement
		if (response.getStatusCode() == HttpStatus.OK) {
			String responseBody = response.getBody();
			
			ObjectMapper mapper = new ObjectMapper();
	        JsonNode root = mapper.readTree(responseBody);

	        JsonNode componentsNode = root.get("components");
	        if (componentsNode.isArray()) {
	            for (JsonNode component : componentsNode) {
	                String name = component.get("name").asText();
	                sonarIssueFiles.add(name);
	                System.out.println("Name: " + name);
	            }
	        }
	        getFileDetail(repoUrl, branch, lastCommits, sonarIssueFiles);
//			System.out.println("Response: " + responseBody);
		} else {
			System.err.println("Failed to fetch file detail. Status code: " + response.getStatusCode());
		}

	}
	
	private void getFileDetail(String repoUrl, String branch, int lastCommits, List<String> sonarIssueFiles) throws Exception {
		List<FileDetail> fileDetailList = new ArrayList<>();
		int projectId = getProjectId(repoUrl);
		for (String file : sonarIssueFiles) {
			FileDetail fileDetail = new FileDetail();
			fileDetail.setCommitDetails(getLatestCommitAuthor(projectId, file, lastCommits, branch));
			fileDetailList.add(fileDetail);
		}
		Files.deleteIfExists(Path.of(OUTPUT_EXCEL_PATH));
		excelWriter.sonarFileDetail(fileDetailList, OUTPUT_EXCEL_PATH);
	}
	
	private List<CommitDetail> getLatestCommitAuthor(int projectId, String filePath, int lastcommits, String branch)
			throws Exception {
		List<CommitDetail> commitDetails = new ArrayList<>();
		HttpClient httpClient = HttpClients.createDefault();
		String apiUrl = gitlabApiUrl + "/projects/" + projectId + "/repository/commits?ref_name=" + branch + "&path="
				+ filePath + "&per_page=" + lastcommits;
		HttpGet request = new HttpGet(apiUrl);
		request.setHeader("Private-Token", accessToken);

		String responseBody = EntityUtils.toString(httpClient.execute(request).getEntity());
		JSONArray commitsArray = new JSONArray(responseBody);

		for (int i = 0; i < commitsArray.length(); i++) {
			JSONObject commit = commitsArray.getJSONObject(i);
			String authorName = commit.getString("author_name");
			String date = commit.getString("committed_date");
			String authorEmail = commit.getString("committed_date");

			CommitDetail commitDetail = new CommitDetail();
			commitDetail.setAuthorName(commit.getString("author_name"));
			commitDetail.setCommitDate(commit.getString("committed_date"));
			commitDetail.setAuthorEmail(commit.getString("author_email"));
			commitDetails.add(commitDetail);
			System.out.println("Conflict file: " + filePath);
			System.out.println("Last commits:");
			System.out.println(", Commit Time : " + date + ", Author Name: " + authorName + "Author Email: " + authorEmail);
		}

		return commitDetails;
	}
	
	private int getProjectId(String repoUrl) throws Exception {
		HttpClient httpClient = HttpClients.createDefault();
		String[] url = repoUrl.split("/");
		String apiUrl = gitlabApiUrl + "/projects/" + url[url.length - 2] + "%2F" + url[url.length - 1];
		HttpGet request = new HttpGet(apiUrl);
		request.setHeader("Private-Token", accessToken);

		String responseBody = EntityUtils.toString(httpClient.execute(request).getEntity());
		JSONObject responseJson = new JSONObject(responseBody);
		return responseJson.getInt("id");
	}

}

package com.example.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;



import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.api.MergeCommand;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.errors.CheckoutConflictException;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.model.CommitDetail;
import com.example.model.ExcelWriter;
import com.example.model.MergeRequestData;

@Service
public class GitService {

	private static final String OUTPUT_EXCEL_PATH = "D:\\demo1\\output1.xlsx";

	@Value("${gitlab.api.url}")
	private String gitlabApiUrl;

	@Value("${gitlab.access.token}")
	private String accessToken;

	@Value("${gitlab.username}")
	private String username;

	@Value("${gitlab.password}")
	private String password;

//	@Autowired
//	private RepoMapper repoConfig;
	@Value("#{'${list.of.repos}'.split(',')}") 
	private List<String> repoUrlToLocalPathMap;

	@Autowired
	private ExcelWriter excelWriter;

	public void getLastCommitOwners(int lastCommits) throws Exception {
		List<MergeRequestData> mergeRequestDataList = new ArrayList<>();
//		List<String> repoUrlToLocalPathMap = repoConfig.getRepositoryUrls();
		for (String repoUrl : repoUrlToLocalPathMap) {

			int projectId = getProjectId(repoUrl);
			mergeRequestDataList.addAll(mergeConflictFile(projectId, repoUrl, lastCommits));
		}
		Files.deleteIfExists(Path.of(OUTPUT_EXCEL_PATH));
		excelWriter.mergeRequestWithMergeConflicts(mergeRequestDataList, OUTPUT_EXCEL_PATH);
	}

	private List<MergeRequestData> mergeConflictFile(int projectId, String repoUrl,
		 int lastCommits) throws Exception {
		List<MergeRequestData> mergeRequestDataList = new ArrayList<>();
		HttpClient httpClient = HttpClients.createDefault();
		String apiUrl = gitlabApiUrl + "/projects/" + projectId + "/merge_requests?state=opened";
		HttpGet request = new HttpGet(apiUrl);
		request.setHeader("Private-Token", accessToken);

		String responseBody = EntityUtils.toString(httpClient.execute(request).getEntity());
		JSONArray mergeRequestsArray = new JSONArray(responseBody);

		// check all the merge requests for particular repository
		for (int i = 0; i < mergeRequestsArray.length(); i++) {
			JSONObject mergeRequest = mergeRequestsArray.getJSONObject(i);
			List<String> filesWithConflicts = new ArrayList<>();
			if (mergeRequest.getBoolean("has_conflicts")) {

				System.out.println("Having merged conflicts in this MR : " + mergeRequest.getString("web_url"));

				int mergeRequestId = mergeRequest.getInt("iid");
				filesWithConflicts.addAll(getFilesWithConflictsForMergeRequest(projectId, mergeRequestId,
						mergeRequest.getString("source_branch"), mergeRequest.getString("target_branch"), repoUrl));

				List<String> commits = new ArrayList<>();
				for (String file : filesWithConflicts) {
					MergeRequestData mergeRequestData = new MergeRequestData();
					mergeRequestData.setMergeRequestUrl(mergeRequest.getString("web_url"));
					mergeRequestData.setConflictFile(file);
					List<CommitDetail> detailList = getLatestCommitAuthor(projectId, file, lastCommits,
							mergeRequest.getString("source_branch"), mergeRequestData);
					System.out.println();

					mergeRequestData.setCommitDetails(detailList);
					mergeRequestDataList.add(mergeRequestData);

					for (String commit : commits) {
//						mergeRequestData.set(mergeRequest.getString("web_url"));
//						mergeRequestData.setMergeRequestUrl(mergeRequest.getString("web_url"));
//						mergeRequestData.setMergeRequestUrl(mergeRequest.getString("web_url"));
						System.out.println(commit);
					}
					System.out.println();
				}
			}
		}
		return mergeRequestDataList;
	}

	private List<String> getFilesWithConflictsForMergeRequest(int projectId, int mergeRequestId, String sourceBranch,
			String targetBranch, String repoUrl)
			throws IOException, InvalidRemoteException, TransportException, GitAPIException {

		List<String> conflictedFiles = new ArrayList<>();
		File tempDirectory = Files.createTempDirectory("git-").toFile();
		Git.cloneRepository().setURI(repoUrl).setDirectory(tempDirectory)
				.setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password)).call();
		System.out.println("Temporary directory: " + tempDirectory.getAbsolutePath());


		// Load the repository from the cloned directory
		Repository repo = new FileRepositoryBuilder().setGitDir(new File(tempDirectory, ".git")).build();

		try (Git git = new Git(repo)) {

			// Set up the CredentialsProvider
			CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(username, password);

			Optional<String> checkoutBranch = git.branchList().setListMode(ListMode.REMOTE).call().stream()
					.map(r -> r.getName()).filter(n -> n.contains(targetBranch)).findAny();
			Optional<String> pullBranch = git.branchList().setListMode(ListMode.REMOTE).call().stream()
					.map(r -> r.getName()).filter(n -> n.contains(sourceBranch)).findAny();

			git.fetch().setCredentialsProvider(credentialsProvider).call();
			git.checkout().setName(checkoutBranch.get()).call();
			
			ObjectId mergeBase = repo.resolve(pullBranch.get());

			// perform the actual merge
			MergeResult merge = git.merge().include(mergeBase).setCommit(true)
					.setFastForward(MergeCommand.FastForwardMode.NO_FF).setMessage("Merged changes").call();
			
			for (Map.Entry<String, int[][]> entry : merge.getConflicts().entrySet()) {
				System.out.println("Key: " + entry.getKey());
				for (int[] arr : entry.getValue()) {
					conflictedFiles.add(entry.getKey());
					System.out.println("value: " + Arrays.toString(arr));
				}
			}

		} catch (CheckoutConflictException ex) {
			List<String> conflictingPaths = ex.getConflictingPaths();
			for (String path : conflictingPaths) {
				conflictedFiles.add(path);
			}
		} catch (Exception ex) {
			System.out.println("::::::::" + ex.getMessage());
		}
		return conflictedFiles;
	}

	private List<CommitDetail> getLatestCommitAuthor(int projectId, String filePath, int lastcommits,
			String sourceBranch, MergeRequestData mergeRequestData) throws Exception {
		List<CommitDetail> commitDetails = new ArrayList<>();
		HttpClient httpClient = HttpClients.createDefault();
		String apiUrl = gitlabApiUrl + "/projects/" + projectId + "/repository/commits?ref_name=" + sourceBranch
				+ "&path=" + filePath + "&per_page=" + lastcommits;
		HttpGet request = new HttpGet(apiUrl);
		request.setHeader("Private-Token", accessToken);

		String responseBody = EntityUtils.toString(httpClient.execute(request).getEntity());
		JSONArray commitsArray = new JSONArray(responseBody);

		for (int i = 0; i < commitsArray.length(); i++) {
			JSONObject commit = commitsArray.getJSONObject(i);
			String commitId = commit.getString("id");
			String authorName = commit.getString("author_name");
			String authorEmail = commit.getString("author_email");
			String date = commit.getString("committed_date");

			CommitDetail commitDetail = new CommitDetail();
			commitDetail.setCommitId(commit.getString("id"));
			commitDetail.setAuthorName(commit.getString("author_name"));
			commitDetail.setAuthorEmail(commit.getString("author_email"));
			commitDetail.setCommitDate(commit.getString("committed_date"));
			commitDetails.add(commitDetail);
			System.out.println("Conflict file: " + filePath);
			System.out.println("Last commits:");
			System.out.println("Commit ID: " + commitId + ", Commit Time : " + date + ", Author Name: " + authorName
					+ ", Author Email: " + authorEmail);
		}

		return commitDetails;
	}

	private int getProjectId(String repoUrl) throws Exception {
		HttpClient httpClient = HttpClients.createDefault();
		String[] url = repoUrl.split("/");
		String apiUrl = gitlabApiUrl + "/projects/" + url[url.length - 2] + "%2F" + url[url.length - 1];
		HttpGet request = new HttpGet(apiUrl);
		request.setHeader("Private-Token", accessToken);

		String responseBody = EntityUtils.toString(httpClient.execute(request).getEntity());
		JSONObject responseJson = new JSONObject(responseBody);
		return responseJson.getInt("id");
	}

}

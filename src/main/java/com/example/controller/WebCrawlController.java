package com.example.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.WebCrawlService;

@RestController
@RequestMapping("/api")
public class WebCrawlController {

	@Autowired
	private WebCrawlService webCrawlService;

	@GetMapping("/web")
	public ResponseEntity<String> getWebPage(@RequestParam String url,
			@RequestParam LocalDate startDate, @RequestParam LocalDate endDate)
			throws Exception {

		webCrawlService.getContent(url, startDate, endDate);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
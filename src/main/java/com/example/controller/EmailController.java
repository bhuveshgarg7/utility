package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.EmailService;

@RestController
@RequestMapping("/api/git")
public class EmailController {

	@Autowired
	private EmailService mailService;

	@PostMapping("/mail")
	public ResponseEntity<String> getFileDetail(@RequestParam String email) throws Exception {

		mailService.sendEmail("abhishekgarg32645@gmail.com", "Subject", "Body");
		return new ResponseEntity<>(HttpStatus.OK);
	}
}

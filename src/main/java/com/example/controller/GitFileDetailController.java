package com.example.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.GitFileDetailService;

@RestController
@RequestMapping("/api/git")
public class GitFileDetailController {

	@Autowired
	private GitFileDetailService gitFileDetailService;
	
	@GetMapping("/getFileDetail")
	public ResponseEntity<String> getFileDetail(@RequestParam int lastCommits, @RequestParam String branchName, @RequestParam String filePath) throws Exception {

		gitFileDetailService.getFileDetail(filePath, branchName, lastCommits);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}

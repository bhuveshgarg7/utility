package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.SonarFileDetailService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
@RequestMapping("/api")
public class SonarFileDetailController {

	@Autowired
	private SonarFileDetailService sonarFileDetailService;

	@GetMapping("/sonar")
	public ResponseEntity<String> getFileDetail(@RequestParam String repoUrl, @RequestParam String projectKey, 
			@RequestParam String branch,@RequestParam int lastCommits) throws Exception {

		sonarFileDetailService.getFileDetail(repoUrl, projectKey, branch, lastCommits);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
}

package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.CommitDetails;
import com.example.service.GitCommitDetailService;

@RestController
@RequestMapping("/api/git")
public class GitCommitDetailsController {

	@Autowired
	private GitCommitDetailService gitcommitDetailService;

	@GetMapping("/getcommitDetail")
	public ResponseEntity<CommitDetails> getcommitDetail(@RequestParam String repoUrl, @RequestParam String branch,
			@RequestParam String authorUsername, @RequestParam String fromDate, @RequestParam String toDate)
			throws Exception {

		CommitDetails response = gitcommitDetailService.getCommitsForAuthor(repoUrl, authorUsername, branch, fromDate, toDate);
		return ResponseEntity.ok(response);
	}
}

package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.service.GitService;

@RestController
@RequestMapping("/api/git")
public class GitController {

	@Autowired
	private GitService gitService;

	@GetMapping("/lastCommitOwners")
	public ResponseEntity<String> getLastCommitOwners(@RequestParam int lastCommits) throws Exception {
		
		gitService.getLastCommitOwners(lastCommits);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
